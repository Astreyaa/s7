CREATE SCHEMA api;
CREATE TABLE api.users
(
    "id"          serial PRIMARY KEY
    , "login"       varchar
    , "password"    varchar
    , "firstname"   varchar
    , "middlename"  varchar
    , "lastname"    varchar
    , "email"       varchar
    , "phone"       varchar
);

COMMENT ON COLUMN api.users.id          IS 'Идентификатор роли в БД';
COMMENT ON COLUMN api.users.login       IS 'Логин';
COMMENT ON COLUMN api.users.password    IS 'Пароль';
COMMENT ON COLUMN api.users.firstname   IS 'Имя';
COMMENT ON COLUMN api.users.middlename  IS 'Отчество';
COMMENT ON COLUMN api.users.lastname    IS 'Фамилия';
COMMENT ON COLUMN api.users.email       IS 'Электронная почта';
COMMENT ON COLUMN api.users.phone       IS 'Телефон';


CREATE TABLE api.friends (
                         id serial PRIMARY KEY ,
                         user_id int,
                         friend_id int,
                         FOREIGN KEY (user_id) REFERENCES api.users (id),
                         FOREIGN KEY (friend_id) REFERENCES api.users (id)
);

COMMENT ON COLUMN api.friends.user_id          IS 'Идентификатор пользователя';
COMMENT ON COLUMN api.friends.friend_id        IS 'Идентификатор друга';