package com.noname.api_user.controllers;

import com.noname.api_user.models.Friends;
import com.noname.api_user.models.User;
import com.noname.api_user.repository.UsersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/api/user/")
public class WebController {


    @Autowired
    UsersRepository usersRepository;

    /**
     * Регистрация
     * @param user
     * @return
     */
    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody User user) {

        if (usersRepository.existsByLogin(user.getLogin())) {
            return ResponseEntity.badRequest().body("Пользователь с таким именем уже существует.");
        }

        usersRepository.save(user);

        return ResponseEntity.ok("Пользователь успешно зарегистрирован.");
    }



    /**
     * Авторизация
     * @param user
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody User user) {

        Optional<User> foundUser = usersRepository.findByLoginAndPassword(user.getLogin(), user.getPassword());

        return foundUser.map(value -> ResponseEntity.ok("Пользователь успешно авторизован.")).orElseGet(() -> ResponseEntity.badRequest().body("Некорректные имя пользователя или пароль."));
    }

    /**
     * Добавление друга или пользователя
     * @param userId
     * @param friendId
     * @return
     */

    @PostMapping("/{userId}/friends/{friendId}")
    public ResponseEntity<String> addFriend(@PathVariable Long userId, @PathVariable Long friendId) {
        User user = usersRepository.findById(userId).orElseThrow();
        User friend = usersRepository.findById(friendId).orElseThrow();

        user.getFriends().add(friend);
        friend.getFriends().add(user);

        usersRepository.save(user);
        usersRepository.save(friend);

        return ResponseEntity.ok("Друг успешно добавлен.");
    }

    /**
     * Список друзей
     * @param userId
     * @return
     */
    @GetMapping("/{userId}/friends")
    public ResponseEntity<List<User>> getFriends(@PathVariable Long userId) {
        Optional<User> user = usersRepository.findById(userId);

        if (user.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        List<User> friends = user.get().getFriends();

        return ResponseEntity.ok(friends);
    }

    /**
     * Удаление из друзей
     * @param userId
     * @param friendId
     * @return
     */
    @DeleteMapping("/{userId}/friends/{friendId}")
    public ResponseEntity<String> removeFriend(@PathVariable Long userId, @PathVariable Long friendId) {
        User user = usersRepository.findById(userId).orElseThrow();
        User friend = usersRepository.findById(friendId).orElseThrow();


        user.getFriends().remove(friend);
        friend.getFriends().remove(user);
        usersRepository.save(friend);
        usersRepository.save(user);

        return ResponseEntity.ok("Друг успешно удален.");
    }
    /**
     * Просмотр всех пользователей
     */

    @GetMapping("/getAll")
    public ResponseEntity<List<User>> list(){
        return  ResponseEntity.ok().body(usersRepository.findAll());
    }
    @GetMapping("/search/{userId}")
    public ResponseEntity<Optional<User>> searchUser(@PathVariable Long userId) {
        return  ResponseEntity.ok().body(usersRepository.findById(userId));
    }

    @GetMapping("/find/{name}")
    public ResponseEntity<User> findByLoginOrName(@PathVariable("name") String name) {
        User user = usersRepository.findByLoginOrName(name);

        if (user != null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
