package com.noname.api_user.repository;

import com.noname.api_user.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("userRepository")
public interface UsersRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

  boolean existsByLogin(String login);

  Optional<User> findByLoginAndPassword(String login, String password);

  Optional<User> findByLogin(String login);

  @Query("SELECT u FROM User u WHERE u.login = :name OR u.lastName = :name")
  User findByLoginOrName( @Param("name") String name);
}