package com.noname.api_user.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Slf4j
@Table(name = "friends", schema = "api")
public class Friends {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "friend_id")
    private User friend;

}
