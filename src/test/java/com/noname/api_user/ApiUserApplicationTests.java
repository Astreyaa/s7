package com.noname.api_user;

import com.noname.api_user.controllers.WebController;
import com.noname.api_user.models.User;
import com.noname.api_user.repository.UsersRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class ApiUserApplicationTests {

    @Test
    void contextLoads() {
    }
    @InjectMocks
    private WebController webController;

    @Mock
    private UsersRepository userRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegisterUser() {
        User user = new User();
        user.setLogin("test_user");
        when(userRepository.existsByLogin(user.getLogin())).thenReturn(false);

        ResponseEntity<String> response = webController.registerUser(user);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Пользователь успешно зарегистрирован.", response.getBody());
    }

    @Test
    public void testRegisterUserExistingUser() {
        User user = new User();
        user.setLogin("test_user");
        when(userRepository.existsByLogin(user.getLogin())).thenReturn(true);

        ResponseEntity<String> response = webController.registerUser(user);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Пользователь с таким именем уже существует.", response.getBody());
    }

    @Test
    public void testLoginUserSuccess() {
        User user = new User();
        user.setLogin("test_user");
        user.setPassword("test_password");
        Optional<User> foundUser = Optional.of(user);
        when(userRepository.findByLoginAndPassword(user.getLogin(), user.getPassword())).thenReturn(foundUser);

        ResponseEntity<String> response = webController.loginUser(user);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Пользователь успешно авторизован.", response.getBody());
    }

    @Test
    public void testLoginUserFailure() {
        User user = new User();
        user.setLogin("test_user");
        user.setPassword("test_password");
        Optional<User> foundUser = Optional.empty();
        when(userRepository.findByLoginAndPassword(user.getLogin(), user.getPassword())).thenReturn(foundUser);

        ResponseEntity<String> response = webController.loginUser(user);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Некорректные имя пользователя или пароль.", response.getBody());
    }

    @Test
    public void testAddFriendSuccess() {
        User user = new User();
        User friend = new User();
        user.setId(1L);
        friend.setId(2L);
        Optional<User> foundUser = Optional.of(user);
        Optional<User> foundFriend = Optional.of(friend);
        when(userRepository.findById(1L)).thenReturn(foundUser);
        when(userRepository.findById(2L)).thenReturn(foundFriend);

        ResponseEntity<String> response = webController.addFriend(1L, 2L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Друг успешно добавлен.", response.getBody());
        assertTrue(user.getFriends().contains(friend));
    }

}
